package com.ep.economia;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DecisionMaker {
    DecimalFormat twoPlaces = new DecimalFormat("0.00");
    private JButton calcularButton;
    private JTextField precoInicial;
    private JTextField precoFinal;
    private JTextField quantidadeInicial;
    private JTextField quantidadeFinal;
    private JPanel decisionView;
    private JTextArea elasticidadePD;
    private JTabbedPane tabbedPane1;
    private JPanel precoDemanda;
    private JPanel teoriaOrcamento;
    private JTextField parametro2;
    private JButton calcularButton2;
    private JTextArea resultado2;
    private JPanel tesouroDireto;
    private JLabel label3;
    private JLabel label2;
    private JTextField parametro3;
    private JButton calcular3;
    private JTextArea resultado3;
    private JTextField investimentos;
    private JTextField gastosGoverno;
    private JTextField exportacoesLiquidas;
    private JTextField consumo;
    private JTextField renda;
    private JRadioButton button2021;
    private JRadioButton button2025;
    private JRadioButton buttonPoupanca;
    private JTextField textField1;
    private JTextField textField2;


    public DecisionMaker() {
        calcularButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elasticidadePD.setText(elasticidadeCruzada());
            }
        });
        calcularButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resultado2.setText(orcamentoEquilibrado());
                //orcamentoEquilibrado();
            }
        });
        calcular3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (button2021.isSelected()) {
                    String resultado = tesouroPrefixado2021(Double.parseDouble(parametro3.getText()));
                    resultado3.setText(resultado);
                }
                if (button2025.isSelected()) {
                    String resultado = tesouroPrefixado2025(Double.parseDouble(parametro3.getText()));
                    resultado3.setText(resultado);
                }
                if (buttonPoupanca.isSelected()) {
                    String resultado = poupanca(Double.parseDouble(parametro3.getText()), Double.parseDouble(textField1.getText()), Integer.parseInt(textField2.getText()));
                    resultado3.setText(resultado);
                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("DecisionMaker");
        frame.setContentPane(new DecisionMaker().decisionView);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    String orcamentoEquilibrado(){
        String resultado = "";

        float c;
        float i;
        float g;
        float nx;
        float r;

        String con = consumo.getText();
        String inv = investimentos.getText();
        String gov = gastosGoverno.getText();
        String exp = exportacoesLiquidas.getText();
        String ren = renda.getText();
        try {
            if (ren.equals("X") || ren.equals("x")) {
                c = Integer.parseInt(con);
                i = Integer.parseInt(inv);
                g = Integer.parseInt(gov);
                nx = Integer.parseInt(exp);

                r = c + i + g + nx;
                System.out.println(r);
                resultado = ("A renda é: " + r);

            } else if (con.equals("X") || con.equals("x")) {

                i = Integer.parseInt(inv);
                g = Integer.parseInt(gov);
                nx = Integer.parseInt(exp);
                r = Integer.parseInt(ren);

                c = -i - g - nx + r;
                resultado = ("O consumo é: " + c);

            } else if (inv.equals("X") || inv.equals("x")) {

                c = Integer.parseInt(con);
                g = Integer.parseInt(gov);
                nx = Integer.parseInt(exp);
                r = Integer.parseInt(ren);

                i = -c - g - nx + r;
                resultado = ("O investimento é: " + i);

            } else if (gov.equals("X") || gov.equals("x")) {

                c = Integer.parseInt(con);
                i = Integer.parseInt(inv);
                nx = Integer.parseInt(exp);
                r = Integer.parseInt(ren);

                g = -c - i - nx + r;
                resultado = ("Os gastos do governo é: " + g);

            } else if (exp.equals("X") || exp.equals("x")) {

                c = Integer.parseInt(con);
                g = Integer.parseInt(gov);
                i = Integer.parseInt(inv);
                r = Integer.parseInt(ren);

                nx = -g - i - c + r;
                resultado = ("As exportações liquidas são: " + nx);

            } else {
                resultado = "Coloque 'X' na variável a qual deseja descobrir";
            }
        }catch (Exception e){
            resultado = "Insira valores numéricos válidos e uma única incógnita 'x'";
        }
        System.out.println(resultado + " hue ");
        return resultado;

    }

    private String elasticidadeCruzada(){
        String resposta;
        try {
            float precoFim = Integer.parseInt(precoFinal.getText());
            float precoIni = Integer.parseInt(precoInicial.getText());
            float quantidadeFim = Integer.parseInt(quantidadeFinal.getText());
            float quantidadeIni = Integer.parseInt(quantidadeInicial.getText());
            double num = ((quantidadeFim - quantidadeIni) / quantidadeIni) / ((precoFim - precoIni) / precoIni);
            if(num < 0) num = num * (-1);
            resposta = "" + num;
        } catch(Exception e){
            resposta = "insira valores validos";
        }
        return resposta;
    }

    public String tesouroPrefixado2021 (double valor) {
        double taxa = 9.20;
        double periodos = this.getDifferenceDays2021()/365.0;
        double juros = Math.pow((1+taxa/100), periodos)-1;
        double valorFinal = valor+(valor*juros);
        String impostoDeRenda = twoPlaces.format((valorFinal-valor)*0.15);
        String valorLiquido = twoPlaces.format((valorFinal-((valorFinal-valor)*0.15)));

        String textoFinal = "Data do resgate: 01/01/2021" +
                "\nValor investido bruto: "+ twoPlaces.format(valor) +
                "\nValor bruto do resgate: "+twoPlaces.format(valorFinal)+
                "\nAlíquota do imposto de renda: 15.00%" +
                "\nImposto de renda: "+ impostoDeRenda+
                "\nValor líquido do resgate: "+ valorLiquido;


        return textoFinal;

    }

    public String tesouroPrefixado2025 (double valor) {
        double taxa = 11.24;
        double periodos = this.getDifferenceDays2025()/365.0;
        double juros = Math.pow((1+taxa/100), periodos)-1;
        double valorFinal = valor+(valor*juros);
        String impostoDeRenda = twoPlaces.format((valorFinal-valor)*0.15);
        String valorLiquido = twoPlaces.format((valorFinal-((valorFinal-valor)*0.15)));

        String textoFinal = "Data do resgate: 01/01/2025" +
                "\nValor investido bruto: "+ twoPlaces.format(valor) +
                "\nValor bruto do resgate: "+twoPlaces.format(valorFinal)+
                "\nAlíquota do imposto de renda: 15.00%" +
                "\nImposto de renda: "+ impostoDeRenda+
                "\nValor líquido do resgate: "+ valorLiquido;


        return textoFinal;
    }

    public double getDifferenceDays2021() {
        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String dateAfterString = "01 01 2021";

        try {
            Date today = new Date();

            Date dateAfter = myFormat.parse(dateAfterString);
            long difference = dateAfter.getTime() - today.getTime();
            double daysBetween = (difference / (1000*60*60*24));
            /* You can also convert the milliseconds to days using this method
             * float daysBetween =
             *         TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS)
             */
            return daysBetween;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    public double getDifferenceDays2025() {
        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String dateAfterString = "01 01 2025";

        try {
            Date today = new Date();

            Date dateAfter = myFormat.parse(dateAfterString);
            long difference = dateAfter.getTime() - today.getTime();
            double daysBetween = (difference / (1000*60*60*24));
            /* You can also convert the milliseconds to days using this method
             * float daysBetween =
             *         TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS)
             */
            return daysBetween;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    public String poupanca (double valor, double taxaSelic, int periodos) {
        double valorFinal;
        double taxaJuros;
        double taxaTR = 0.0878;
        if (taxaSelic <= 8.5) {
            taxaJuros = ((taxaSelic/100)*0.70)+(taxaTR/100);
            double juros = Math.pow((1+taxaJuros), periodos)-1;
            valorFinal = valor+(valor*juros);
        } else {
            taxaJuros = (taxaTR/100)+(0.5/100);
            double juros = Math.pow((1+taxaJuros), periodos)-1;
            valorFinal = valor+(valor*juros);
        }
        Date today = new Date();
        String formato = "dd/MM/yyyy";
        SimpleDateFormat dataFormatada = new SimpleDateFormat(formato);
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.add(Calendar.MONTH, periodos);

        String impostoDeRenda = twoPlaces.format((valorFinal-valor)*0.15);
        String valorLiquido = twoPlaces.format((valorFinal-((valorFinal-valor)*0.15)));

        String stringFinal = "Data do resgate: "+dataFormatada.format(cal.getTime())+
                "\nTaxa Selic: "+taxaSelic+"%"+
                "\nValor investido bruto: "+ twoPlaces.format(valor) +
                "\nValor bruto do resgate: "+twoPlaces.format(valorFinal)+
                "\nAlíquota do imposto de renda: 15.00%" +
                "\nImposto de renda: "+ impostoDeRenda+
                "\nValor líquido do resgate: "+ valorLiquido;
        return stringFinal;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}


